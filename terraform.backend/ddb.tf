resource "aws_dynamodb_table" "tf_ddb" {
  name           = "project4-state-ddb"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "project4-state-ddb"
  }
}

