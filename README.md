**Project Goal:**
As a DevOps Engineer, I want to automate the deployment process of resources( VPC, Private subnet, Public subnet, EC2 instance (free tier) in each subnet, Route tables (public, private), Internet, Gateway, Security Group, Ingress rules for the SG ) using Terraform and an S3 backend so that we can ensure consistent and error-free deployments for our infrastructure.

**Services and Components:**
1. **Terraform:** will be used for to write the codes for infrastructure provisioning.
2. **S3 Bucket:** to store the state of our terraform file.
3. **DynamoDB table:** for locking file to prevent write operations.

**Project Steps:**

**1. Setup:**
    Install Visual Studio Code and Terraform

   https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

   https://learn.microsoft.com/en-us/visualstudio/install/install-visual-studio?view=vs-2022 

   Under Extensions, search for Terraform (Hashicorp) plugin and install it.

**2. Create files:**

    - Create a provider
    https://registry.terraform.io/providers/hashicorp/aws/latest/docs
    - Create VPC
    - Create public subnet
    - Create private subnet
    - Create NAT Gateway and Elastic IP
    - Create Public Route Table
    - Associate public subnet with public route table
    - Create Private Route Table
    - Associate Private subnet with private route table
    - Create Security Groups
    - EC2 instances
    - Create and setup S3 backend



